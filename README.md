# YouTube Video Downloader

This is a command-line script written in Python that downloads YouTube videos. It uses the `pytube` library to access and download the video and audio streams from YouTube, and `moviepy` to combine these streams into a single MP4 file.

## Terms

Before downloading any content from this repository, please ensure that you review and comply with the YouTube Terms of Service. The content available on YouTube is governed by their terms and conditions, and it is essential to respect and adhere to these guidelines.

Downloading or using YouTube content without proper authorization or in violation of their terms may have legal implications. It is your responsibility to understand any licensing restrictions, copyright issues, or usage limitations imposed by YouTube on their content.

Please note that this repository is not affiliated with YouTube and assumes no liability for your usage of YouTube content. By downloading and using any content from this repository, you agree to assume full responsibility for complying with YouTube's Terms of Service.

For more information, please visit the [YouTube Terms of Service](https://www.youtube.com/t/terms) page.

## Features

- Downloads video and audio streams separately for efficient downloading.
- Combines video and audio into a single MP4 file.
- Allows for choosing the video quality (if available).
- Sanitizes the video title to remove invalid characters and uses it as the output filename.
- Includes the video ID as a prefix in the filename.
- Prints a progress bar while downloading.

## Usage

The script can be run directly from the command line:

```bash
python youtube_downloader.py <video_url> [-quality <quality>]
```

## Dependencies
The script requires the following Python libraries:
- pytube
- moviepy

You can install these libraries using pip:

```shell
pip install pytube moviepy
```

## Known Issues
This script has a known issue: if a temporary file TEMP_MPY_wvf_snd.mp3 is created and not removed during the video and audio combination process, the script will attempt to delete it after the final video file is created.

## Contributing
Feel free to fork the repository and submit pull requests for any changes or improvements you make.

## License
This script is released under the MIT License.