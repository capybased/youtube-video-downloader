import os
import sys
from pytube import YouTube
from pytube.cli import on_progress
from moviepy.editor import VideoFileClip, AudioFileClip

# Function to sanitize filenames by removing invalid characters
def sanitize_filename(filename):
    invalid_chars = ['/', '\\', ':', '*', '?', '"', '<', '>', '|']
    for char in invalid_chars:
        filename = filename.replace(char, '')
    return filename

# Function to download videos from YouTube
def download_video(url, quality=None):
    try:
        # Create a YouTube object with the provided URL
        yt = YouTube(url, on_progress_callback=on_progress)

        # Select the video stream based on the provided quality
        if quality:
            video_stream = yt.streams.filter(file_extension='mp4', res=quality).order_by('resolution').desc().first()
        else:
            video_stream = yt.streams.filter(file_extension='mp4').order_by('resolution').desc().first()

        # Check if video stream is available
        if video_stream is None:
            print("The requested quality is not available for this video.")
            return

        # Get the audio stream from the video
        audio_stream = yt.streams.get_audio_only()
        video_filename = 'video.mp4'
        audio_filename = 'audio.mp3'

        # Download the video and audio streams
        video_stream.download(filename=video_filename)
        audio_stream.download(filename=audio_filename)

        # Combine video and audio
        video_clip = VideoFileClip(video_filename)
        audio_clip = AudioFileClip(audio_filename)
        final_clip = video_clip.set_audio(audio_clip)

        # Sanitize and set the final file name
        sanitized_title = sanitize_filename(yt.title)
        # Add the video ID as a prefix to the filename
        final_filename = f"{yt.video_id} - {sanitized_title}.mp4"
        final_clip.write_videofile(final_filename)

        # Remove temporary files
        os.remove(video_filename)
        os.remove(audio_filename)
        if os.path.exists('TEMP_MPY_wvf_snd.mp3'):
            os.remove('TEMP_MPY_wvf_snd.mp3')

        print('\nVideo downloaded successfully.')
    except Exception as e:
        print(f'Error: {str(e)}')

# Check if the script is being run directly
if __name__ == '__main__':
    # Check for correct usage
    if len(sys.argv) < 2:
        print('Usage: python youtube_downloader.py <video_url> [-quality <quality>]')
        sys.exit(1)

    # Get the video URL from command line arguments
    video_url = sys.argv[1]
    quality = None
    if "-quality" in sys.argv:
        try:
            # Get the quality from command line arguments
            quality = sys.argv[sys.argv.index("-quality")+1]
        except IndexError:
            print("You must specify a quality after the -quality flag.")
            sys.exit(1)

    # Call the download video function
    download_video(video_url, quality)